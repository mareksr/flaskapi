from flask_sqlalchemy import SQLAlchemy
from flask import Flask,jsonify,request
from flask_marshmallow import Marshmallow

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://szmejk:dupa.123@localhost/hb3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True


db = SQLAlchemy()
db.init_app(app)
ma = Marshmallow(app)



class Customer(db.Model):
  __tablename__ = 'customer'
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(100), nullable=True)

  def __init__(self, id=None, name=None):
    self.id = id
    self.name = name
   

class CustomerSchema(ma.Schema):
  class Meta:
    model = Customer
    fields = ('id','name')


