from flask import Flask,render_template,jsonify,request,flash,redirect,url_for,session,logging
from data import Articles
from flask_mysqldb import MySQL
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

import subprocess

app = Flask(__name__)


app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://szmejk:dupa.123@localhost/hb3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)
ma = Marshmallow(app)


Articles = Articles()

class Customer(db.Model):
  __tablename__ = 'customer'
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(100), nullable=True)

class CustomerSchema(ma.Schema):
  class Meta:
    model = Customer
    fields = ('id','name')



@app.route('/')
def index():
  return render_template('home.html')

@app.route('/about')
def about():
  return render_template('about.html')

@app.route('/articles')
def articles():
  return render_template('articles.html', articles = Articles )

@app.route('/article/<string:id>')
def article(id):
  return render_template('article.html', id=id )

@app.route('/_get_current_user', methods = ['GET','POST'])
def get_current_user():
  if request.method == 'GET':
    customers_schema = CustomerSchema(many=True)
    customers= Customer.query.all()
    result = customers_schema.dump(customers)
    return jsonify(result.data)

  elif request.method == 'POST':
    return "add user"

@app.route('/status')
def status():
   result_success = subprocess.check_output(["uptime"], shell=True)
   return result_success

if __name__ == '__main__':
  app.debug = True
  app.run(host='0.0.0.0', port=5000)

