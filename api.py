from functools import wraps
from flask import Flask,jsonify,request
from flask import request, abort
from model import Customer,CustomerSchema,db,ma,app



def require_appkey(view_function):
    @wraps(view_function)
    # the new, post-decoration function. Note *args and **kwargs here.
    def decorated_function(*args, **kwargs):
        if request.args.get('key') and request.args.get('key') == 'VtoCglxw1ddxOtjwoAHYp809RNkHkVpz':
            return view_function(*args, **kwargs)
        else:
            abort(401)
    return decorated_function



@app.route('/customers', methods = ['GET','POST'])
@require_appkey
def customers():
  if request.method == 'GET':
    customers_schema = CustomerSchema(many=True)
    customers= Customer.query.all()
    result = customers_schema.dump(customers)
    return jsonify(result.data)


@app.route('/customer', methods = ['POST'])
@require_appkey
def customer():
  if request.method == 'POST':
    if not request.json or not 'name' in request.json:
        abort(400)
    db.create_all()
    customer= Customer(name=request.json.get('name',''))
    db.session.add(customer)
    db.session.commit()
    return 'JSON posted'


if __name__ == '__main__':
  app.debug = True
  app.run(host='0.0.0.0', port=80)

